= Cloud Adoption
Eric D. Schabell @eschabell, Iain Boyle @iainboy
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left

_Some details will differ based on the requirements of a specific implementation but all portfolio architectures generalize one or more successful deployments of a use case._


*Use case:* Accelerating cloud adoption with effective automation for deploying and managing workloads across multiple
cloud infrastructures according to performance, security, compliance, and cost requirements.

*Background:* 
As enterprises transition to public and/or private clouds, it is important to
provide automation to manage and scale server deployments and to provide the capability to transition servers between
data centers and cloud providers. This provides flexibility and portability.

== Solution overview

====
*Cloud Adoption*

. Consistency of RHEL estate across all public clouds and datacenters
. Quickly scale cloud deployments 
. Remote management and automation
====

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/cloud-adoption-marketing-slide.png[750,700]
--

== Logical diagram

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/cloud-adoption-ld.png[750, 700]
--

== The technology

The following technology was chosen for this solution:

====
https://www.redhat.com/en/technologies/management/satellite?intcmp=7013a00000318EWAAY[*Red Hat Satellite*]  is the smart management element in this architecture, used for tracking, managing, auditing, and
collecting data on the entire infrastructure to ensure that baselines are met.

https://www.redhat.com/en/technologies/management/smart-management?intcmp=7013a00000318EWAAY[*Red Hat Smart Management*] provides a cloud experience, existing physical data center resources might be the starting
point to be offered to the organization with a cloud-like experience. This combines the flexible and powerful
infrastructure management capabilities with the ability to execute remediation plans. It helps to securely manage any
environment supported by Red Hat Enterprise Linux, from physical machines to hybrid multiclouds.

https://www.redhat.com/en/technologies/management/ansible?intcmp=7013a00000318EWAAY[*Red Hat Ansible Automation Platform*] facilitates consistent,repeatable,and tested infrastructure automation tasks as
needed by the other elements managing the hybrid cloud. This element is directed to execute based on the findings of
the smart management element.

https://www.redhat.com/en/technologies/cloud-computing/quay?intcmp=7013a00000318EWAAY[*Red Hat Quay*] is part of the Core Data Center, it is a private container registry that stores, builds, and deploys container
images. It analyzes your images for security vulnerabilities, identifying potential issues that can help you mitigate
security risks.

https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux?intcmp=7013a00000318EWAAY[*Red Hat Enterprise Linux*] (RHEL) is the host element with an image registry to facilitate the deployment of infrastructure,
services, and applications across the entire hybrid cloud infrastructure.

https://www.redhat.com/en/technologies/management/?intcmp=7013a00000318EWAAY[*Red Hat Insights*] is the key to monitoring and data collection around the entire hybrid cloud infrastructure. Based on
this data and working together with insights services, automated actions can take place around updates, security patches,
infrastructure rollouts, workload management, and workload migrations. This is the key to an organizations ability to
successfully adopt a truly hybrid cloud infrastructure.
====

== Architectures
=== Cloud Adoption overview
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/cloud-adoption-network-sd.png[750, 700]
--

This overview looks at Cloud Adoption, providing the solution details and the elements described above in both a
network and data centric view:

It starts in the core data center where images are built (where the application source code are in a
source code management system (SCM)) and deployed out to the image registry found in a physical data center, a private
cloud, or in any public clouds desired. Quay is used to sync these registries.

Cloud services assist with analyzing the data to help manage responses and maintain a repository of automated actions.
Result of the analyzed data react to specific insights with plans that can be used to support the infrastructure
management.

Infrastructure management uses smart management to monitor all deployments and locations, leveraging input from the
cloud services provided by insights and automation repositories. If needed, remediation can be triggered by smart
management and automation orchestration will take action as defined in the automation playbooks to fix deployments.

Infrastructure management also uses the gained workload insights to deploy new updates and manage security patches
across all infrastructure destinations. 


=== Cloud Adoption data overview
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/cloud-adoption-data-sd.png[750, 700]
--

== Download solution diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/cloud-adoption.drawio[[Open Diagrams]]
--

== Provide feedback on this architecture
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/cloud-adoption.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].

